package org.elu.learn.groovy

import groovy.transform.ToString

// Creating Lists and Sets
def l = [1, 3, 5, 7] // default type is ArrayList
println l
println l.getClass()

l = [1, 3, 5, 7] as LinkedList
println l
println l.getClass()

l = [1, 3, 5, 7] as Set // default type for Set is LinkedHashSet
println l
println l.getClass()

l = [1, 3, 5, 7] as HashSet
println l
println l.getClass()
println()

// Creating Maps
def m = [a: 1, b: 5, f: 17] // default type is LinkedHashMap
println m
println m.getClass()
println()

// Composing Collections of Different Types
def food = [fruits: ['apple', 'banana', 'orange'], vegitables: ['potato', 'green beans']]
println food
println food.getClass()
println()

// Accessing Elements of a List
l = ['a', 'b', 'c', 'd', 'e', 'f']
println l.get(0)
println l[0]
println l[1..3]
println()

// Using Groovy Truthiness with Collections
println l.size()
println l.isEmpty()
boolean hasElements = l
println hasElements
hasElements = []
println hasElements
hasElements = null
println hasElements
println()

// Creating and Accessing Composite Collections
m = [
    boys : ['Arno', 'Lauri'],
    girls: ['Alisa', 'Karoliina']
]
println m['girls']
println m['boys']
println m['girls'][0]
println m['boys'][1]
println()

// Processing Lists and Sets
l = [1, 2, 3, 4, 5]
l.each {print "$it "}
println()
println()

// Processing Lists and Sets by Index
l.eachWithIndex{ el, idx -> println "Current element = $el, iteration # = $idx" }
l = [1, 2, 3, 4, 5] as Set
l.each {print "$it "}
println()
println()

// Processing Maps by Key and Value
m = [
    'Monday': ['Watch courses', 'Do exercise'],
    'Friday': ['Relax', 'Spend time with Family', 'Walk dog']
]
println m
m.each {k, v ->
    println "$k = $v"
}
for (entry in m) {
    println "Key = $entry.key"
    println "Value = $entry.value"
}
println()

// Filtering Collections
def numbers = [1, 2, 3, 4, 5]
println numbers
println numbers.findAll {e -> e % 2 == 1 }
println numbers.findAll {it % 2 == 1 }

def names = ['Arno', 'Lauri', 'Alisa', 'Karoliina']
println names
println names.findAll {it.startsWith('A')}

@ToString
class Person {
    String name
    int age
}
def namesAndAges = [
    new Person(name: 'Robert', age: 50),
    new Person(name: 'Simon', age: 45),
    new Person(name: 'Suzie', age: 32),
]
println namesAndAges
namesAndAges.findAll { it.age >= 40 }.each {println 'Name: ' + it.name }
println()

// Finding the Matching Element in a Collection
println namesAndAges.findAll {it.age >= 40 }
println namesAndAges.find {it.age >= 40 }?.name
println()

// Testing Elements in Collections
println namesAndAges.every {it.age >= 40 }
println namesAndAges.every {it.age >= 30 }
println namesAndAges.any {it.age >= 40 }
println namesAndAges.any {it.age >= 60 }
println()

// Collecting Elements to a List
println l
println l.collect { it * 2 }
println names
println names.collect { it.toUpperCase() }
println namesAndAges
println namesAndAges.collect { it.name.toUpperCase() }
println()

// Collecting Entries to a Map
println namesAndAges.collectEntries { [(it.age): it.name.toUpperCase()] }
println()

// Creating Aggregate Functions with Inject
println numbers
println numbers.sum()
println numbers.min()
println numbers.max()
println numbers.inject(0) { sum, el -> sum + el }
println numbers.inject(0) { max, el -> Math.max(max, el) }
