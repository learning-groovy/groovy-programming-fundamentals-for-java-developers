package org.elu.learn.groovy

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

// Default Imports and Classes
def number = 5.6
println number
println number.getClass()

number = 123
println number
println number.getClass()

number = 123G
println number
println number.getClass()
println()

// Creating Classes and Instances in Groovy
class Person {
    String name

//    Person(String name) {
    Person(name) {
        this.name = name
    }

    def greet() {
        println "Hello, I'm $name!"
    }
}
Person p = new Person('Edu')
println p
println()

// Adding methods and state to Groovy Classes
p.greet()
println()

// Using Inheritance in Groovy
class Employee extends Person {
    int salary

    Employee(String name, int salary) {
        super(name)
        this.salary = salary
    }

    static def reportForWork() {
        'Here sir, ready and willng for the long day ahead!'
    }
    // Overriding Methods in Groovy
    @Override
    def greet() {
        println "Hello, I'm $name, I earn $salary"
    }
}
def e = new Employee('Finn', 25000)
e.greet()
println e.reportForWork()
println()

// POGOs and Groovy Property Generation
class Personn {
    String name
}
def p1 = new Personn()
println p1.name
p1.name = 'Edu'
println p1.name
println()

// Operator Overloading
@ToString
class Greeting {
    String message

    Greeting positive() {
        return new Greeting(message: this.message.toUpperCase())
    }
}
def g = new Greeting(message: 'Hello')
println g
println(+g)
println()

// String Equality in Groovy
String s1 = 'Hello'
println s1
String s2 = 'Hi'
println s2
println s1.equals(s2) // Java style String comparison
println s1 == s2 // in Groovy this is equivalent to above
println()

// Returning Multiple Values from a Method
@ToString(includeNames = true)
class BoxDimensions {
    int x, y, z
}
// In Java to return multiple values, they must be wrapped into Object
static BoxDimensions calculate() {
    // do some calculation...
    // ...then return the dimensions
    new BoxDimensions(x: 10, y: 12, z: 50) // in Groovy last statement is returned by default
}
BoxDimensions dimensions = calculate()
println dimensions
int area = dimensions.x * dimensions.y
println area

// In Groovy method can return multiple values
static def calculate2() {
    [10, 12, 30]
}
def (x, y, z) = calculate2()
println "x: $x, y: $y, z: $z"
int area2 = x * y
println area2
println()

// Autogenerating Equals and HashCode with Groovy AST Transformations
@ToString
@EqualsAndHashCode
class Henkilo {
    String name
    int age
}
def h = new Henkilo()
println h
println()

// Named Constructors
println new Henkilo()
println new Henkilo(name: 'Luna')
println new Henkilo(age: 5)
println new Henkilo(name: 'Luna', age: 5)
println new Henkilo(age: 3, name: 'Funny')
