import groovy.transform.ToString

// Null-Safe Dereference Operator
def s = 'Hello'
println s.toUpperCase()

s = null
println s?.toUpperCase() // ?. null dereference operator

@ToString
class Job {
    String roleName
    int salary
}
@ToString
class Person {
    String name
    Job job
}
@ToString
class Department {
    String depName = 'Engineering'
    Set<Person> staff = [new Person(name: 'Edu', job: new Job(roleName: 'Developer', salary: 32000)),
                         new Person(name: 'Finn', job: new Job(salary: 38000))]
}
def dept = new Department()
println "${dept.staff[0].job.roleName} salary: ${dept.staff[0].job.salary}" // this line fails with NullPointerException
// println "Manager salary: ${dept.staff[1].job.salary}" // this line fails with NullPointerException
println "${dept.staff[1]?.job?.roleName?.toUpperCase()} salary: ${dept.staff[1]?.job?.salary}" // this line runs fine
println "${dept.staff[2]?.job?.roleName?.toUpperCase()} salary: ${dept.staff[2]?.job?.salary}"
println()

// Elvis Operator
def displayUsername(String loggedInUser) {
    println loggedInUser ?: 'Guest'
}
displayUsername('Arno')
displayUsername(null)
displayUsername('')
println "${dept.staff[1]?.job?.roleName?.toUpperCase() ?: 'Manager'} salary: ${dept.staff[1]?.job?.salary}"
println "${dept.staff[2]?.job?.roleName?.toUpperCase() ?: 'Manager'} salary: ${dept.staff[2]?.job?.salary}"
println()

// Spaceship Operator
println 'Arno' <=> 'Lauri'
println 'Lauri' <=> 'Arno'
println 'Arno' <=> 'Arno'

def colours = ['black', 'white', 'blue', 'orange']
println colours
println colours.sort()
println colours.sort {a, b -> a <=> b }
println colours.sort {a, b -> b <=> a }

@ToString
class FoodOrder {
    String name
    BigDecimal cost

    FoodOrder(name, cost) {
        this.name = name
        this.cost = cost
    }
}
def driveThruOrder = [
        new FoodOrder('Burger', 3.99),
        new FoodOrder('Fries', 1.85),
        new FoodOrder('Beer', 2.75),
]
println driveThruOrder
println driveThruOrder.sort()
println driveThruOrder.sort {a, b -> a.name <=> b.name }
println driveThruOrder.sort {a, b -> a.cost <=> b.cost }
println driveThruOrder.sort {a, b -> b.cost <=> a.cost }
println()

// Spread Operator
def fruits = ['apples', 'oranges', 'pears']
println fruits
def shoppingList = ['milk', 'bread', *fruits]
println shoppingList

static void capitaliseThreeStrings(String a, String b, String c) {
    println 'A = ' + a
    println 'B = ' + b
    println 'C = ' + c
}
capitaliseThreeStrings(*fruits)

static void capitaliseStrings(String... args) {
    args.each {
        println 'Element = ' + it
    }
}
fruits << 'bananas'
capitaliseStrings(*fruits)
println fruits*.toUpperCase()
println()

// Range Operator
println 1..5
println((1..5).getClass())
println((1..5)[1])
println((1..5).last())
(1..5).each { print "${it} " }
println()
(1..<5).each { print "${it} " }
println()

enum Weekdays {
    MON, TUES, WEDS, THURS, FRI
}
println((Weekdays.TUES..Weekdays.THURS).getClass())
(Weekdays.TUES..Weekdays.THURS).each {print "${it} " }
println()
(Weekdays.TUES..<Weekdays.THURS).each {print "${it} " }
println()
