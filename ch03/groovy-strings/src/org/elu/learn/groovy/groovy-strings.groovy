package org.elu.learn.groovy

// String Interpolation
def s = 'How are you?' + ' Good?'
println s
println s.getClass()

def name = 'Edu'
s = "How are you, $name?"
println s
println s.getClass()
println()

// Heredocs
def emailText = '''
Hi there!

Thanks for signing up. You're awesome!

Have a great day!

The Groovy Team
'''
print emailText
println emailText.getClass()

emailText = """
Hi there, $name!

Thanks for signing up. You're awesome!

Have a great day!

The Groovy Team
"""
println emailText
println emailText.getClass()
println()

// Pattern Operator (~) in Regular Expressions
def re = ~'S.*' // will match words like Sugar, Sweet, Syrup
println re.getClass()

def matcher = re.matcher('Sweet')
println matcher.getClass()
println matcher.matches()
println()

// Slashy Regular Expressions (//)
re = ~/\d*/ // with single quotes this will require escaping for backslash
matcher = re.matcher("123")
println matcher.matches()
println re.matcher("abc").matches()
println()

// Find Operator (=~) in Regular Expressions
println(('Sugar' =~ /S.*/).matches())
println(('Sugar' =~ /L.*/).matches())
println()

// Match Operator (==~) in Regular Expressions
def matches = 'Sugar' ==~ /S.*/
println matches
println 'Sugar' ==~ /L.*/
println()

// Capture Groups in Regular Expressions
s = /Friday is Edu's favourite day!/
def captures = (s =~ /(.*) is (.*) favourite day!/)[0]
println captures
def dayOfWeek = captures[1]
def whoseDay = captures[2]
println 'Day of week: ' + dayOfWeek
println 'Whose day: ' + whoseDay
println()
